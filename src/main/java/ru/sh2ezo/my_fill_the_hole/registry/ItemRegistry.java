package ru.sh2ezo.my_fill_the_hole.registry;

import net.minecraft.item.Item;
import ru.sh2ezo.my_fill_the_hole.items.HoleFillerItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ItemRegistry {
    private List<Item> items = new ArrayList<Item>();

    public ItemRegistry() {
        items.add(new HoleFillerItem());
    }

    public Collection<Item> getItems() {
        return Collections.unmodifiableCollection(items);
    }
}
