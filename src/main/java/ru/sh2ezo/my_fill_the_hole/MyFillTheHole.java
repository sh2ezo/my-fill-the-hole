package ru.sh2ezo.my_fill_the_hole;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import ru.sh2ezo.my_fill_the_hole.registry.ItemRegistry;

@Mod(modid = MyFillTheHole.ModId, version = "1.0")
public class MyFillTheHole {
    public final static String ModId = "my_fill_the_hole";

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

    }

    @Mod.EventHandler
    public void preLoad(FMLPreInitializationEvent event) {
        for (Item item : new ItemRegistry().getItems()) {
            GameRegistry.registerItem(item, item.getUnlocalizedName());
        }
    }
}
