package ru.sh2ezo.my_fill_the_hole.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFireball;
import net.minecraft.item.ItemSnowball;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.sh2ezo.my_fill_the_hole.MyFillTheHole;
import ru.sh2ezo.my_fill_the_hole.entities.HoleFillerEntity;

public class HoleFillerItem extends Item {

    public HoleFillerItem() {
        super();
        this.setMaxDamage(0);
        this.setCreativeTab(CreativeTabs.tabMisc);
        this.setUnlocalizedName("hole_filler");
        this.setTextureName(MyFillTheHole.ModId + ":" + getUnlocalizedName());
    }

    @Override
    public ItemStack onItemRightClick(ItemStack items, World world, EntityPlayer player) {
        super.onItemRightClick(items, world, player);

        if (world.isRemote) {
            world.spawnEntityInWorld(new HoleFillerEntity(world, player));
        }

        items.splitStack(1);

        return items;
    }
}
