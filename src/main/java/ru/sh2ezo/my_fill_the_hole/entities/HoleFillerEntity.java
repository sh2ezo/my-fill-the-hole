package ru.sh2ezo.my_fill_the_hole.entities;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class HoleFillerEntity extends EntityThrowable {

    public HoleFillerEntity(World world, EntityLivingBase thrower) {
        super(world, thrower);
    }

    @Override
    protected void onImpact(MovingObjectPosition pos) {
        worldObj.newExplosion(null, pos.blockX, pos.blockY, pos.blockZ, 10, true, true);
    }
}
